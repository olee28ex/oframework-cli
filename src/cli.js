import arg from 'arg';
import inquirer from 'inquirer';
import figlet from 'figlet';
import chalk from 'chalk';
import grd from 'gradient-string';
import { createProject } from './main';


require('pkginfo')(module, 'version');
const info = module.exports;
            
const logo = figlet.textSync(" Oframework", {font: "Nancyj-Fancy", horizontalLayout: "default", verticalLayout: "default" })

function parseArgumentsIntoOptions(rawArgs) {
    const args = arg(
        {
            '--git': Boolean,
            '--yes': Boolean,
            '--install': Boolean,
            '--version': Boolean,
            '-g': '--git',
            '-y': '--yes',
            '-i': '--install',
            '-v': '--version'
        },
        {
            argv: rawArgs.slice(2),

        }
    );
    return {
        skipPrompts: args['--yes'] || false,
        git: args['--git'] || false,
        version: args['--version'] || false,
        command: args._[0],
        appname: args._[1],
        model: args._[2],
        runInstall: args['--install'] || false
    };
}

async function promptForMissingOptions(options) {
    const defaultTemplate = 'MongoDB';
    if(options.skipPrompts) {
        return {
            ... options,
            template: options.template || defaultTemplate,
        }

    }
    const questions = [];
    if( !options.template ) {
        questions.push({
            type: 'list',
            name: 'template',
            message: 'Please choose which DB Engine to use',
            choices: ['MongoDB', 'Mysql/MariaDB', 'Postgresql'],
            default: defaultTemplate
        });
    }

    if( !options.git ) {
        questions.push({
            type: 'confirm',
            name: 'git',
            message: 'Initialize a git repository?',
            default: false
        });
    }

    const answers = await inquirer.prompt(questions);
    return {
        ... options,
        template: options.template || answers.template,
        git: options.git || answers.git,
    }
}

function showVersion() {
    console.log('\n');
    console.log(chalk.rgb(0,255,255)('  ===============================================================================================\n'));
    console.log(
        grd([
            { color: '#00FFFF', pos: 0},
            { color: '#0088FF', pos: .2},
            { color: '#0088FF', pos: .7},
            { color: '#00FFFF', pos: 1}
        ])
        (logo)
    );
    console.log(chalk.rgb(0,255,255)('  ===============================================================================================\n'));
    console.log('\n');
    console.log(chalk.bold(`  ${chalk.greenBright.bold('Oframework')} : ${info.version}\n`));
    console.log('\n  Commands:\n')
    commandDesc('new         ', '[API_Name]' , 'Generate new API project.');
    commandDesc('Generate    ', '[API_Name]' , 'Generate new API project.');
    commandDesc('  entity    ', '[Name]' , 'Generate an Entity.', true);
    commandDesc('  controller', '[Name]' , 'Generate a Controller.', true);
    commandDesc('  middleware', '[Name]' , 'Generate a Middleware.', true);
    commandDesc('  router    ', '[Name]' , 'Generate a Router.', true);
    console.log('\n  params:\n')
    paramDesc('-v, --version', 'Show Oframework version.');
    paramDesc('-y, --yes', '    Default Install.');
    paramDesc('-g, --git', '    Initialize Git on project.');
    paramDesc('-i, --install', 'Install node dependencies.');
    paramDesc('    --db', '     Change default DB engine (MongoDB).');
}

function paramDesc(cmd, desc) {
    let c = `   ${ chalk.blue.bold(cmd) }  ${desc}`;
    console.log(c);
}

function commandDesc(cmd, val, desc, sw) {
    let c = `   ${ chalk.blue.bold(cmd) } ${ chalk.cyan(val) }  ${desc}`;
    c += sw ? ' - '+chalk.rgb(255,165,0)(' Not available now!') : '';
    console.log(c);
}

function ln() { console.log('\n'); }

function chkCommand(options) {
    switch (options.command) {
        case 'new':
            newProject(options);
            break;    
        default:
            break;
    }
}

async function newProject(options) {
    console.log('  Generating API Project ...');
    if( !options.model ) {
        if(!options.appname) {
            ln();
            const prompt = await inquirer.prompt([
                { name: 'name', message: 'What is API Name?' }
            ]);
            options.appname  = prompt.name;
        }
        if(!options.appname) {
            console.log(chalk.red('\nNote: The API name is required!'));
            return true;
        } else {
            options = await promptForMissingOptions(options);
            console.log(options);
            createProject(options);
        }
    }
}

export async function cli(args) {
    // console.log(args.slice(2));
    if(args.length < 3) {
        showVersion();
        process.exit(1); return true;
    }
    let options = parseArgumentsIntoOptions(args);
    if(options.version) {
        ln();
        console.log(chalk.bold(`  ${chalk.blueBright.bold('Oframework-cli')} : ${info.version}`));
        console.log(chalk.bold(`  ${chalk.greenBright.bold('Oframework-core')} : N/A.`));
        console.log(chalk.bold(`  ${chalk.greenBright.bold('Oframework-ng')} : N/A.`));
        console.log(chalk.bold(`  ${chalk.greenBright.bold('Oframework-util')} : N/A.`));
        console.log(chalk.bold(`  ${chalk.greenBright.bold('Oframework-mobile')} : N/A.`));
    } else {
        chkCommand(options);
    }

    return true;
}