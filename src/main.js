import chalk from 'chalk';
import fs from 'fs';
import ncp from 'ncp';
import path from 'path';
import { promisify } from 'util';
import execa from 'execa';
import Listr from 'listr';
import { projectInstall } from 'pkg-install';

const access = promisify(fs.access);
const copy = promisify(ncp);

async function copySeedFiles(options) {
    return copy(
        options.projectDirectory,
        `${options.targetDirectory}/${options.appname}`, 
        {
            clober: false 
        }
    );
}

async function initGit(options) {
    const results = await execa('git', ['init'], {
        cwd: options.targetDirectory,
    });
    if(results.failed) {
        return Promise.reject(new Error('Failed to initialize Git'));
    }
    return;
}

export async function createProject(options) {
    console.log('Creating Project!!');
    options = {
        ... options,
        targetDirectory: options.targetDirectory || process.cwd(),
    };

    const currentFileUrl = import.meta.url;
    const proyectDir = path.resolve(__dirname, '../seedfiles');
    options.projectDirectory =  proyectDir;

    try {
        await access(`${proyectDir}`, fs.constants.R_OK);
    } catch (error) {
        console.log(error);
        console.log('%s Invalid Directory or path', chalk.red.bold('ERROR'));
        process.exit(1);
        
    }
    console.log(`${options.targetDirectory}/${options.appname}`);
    const tasks = new Listr([
        {
            title: 'Copy projec files.',
            task: () => copySeedFiles(options)
        },
        {
            title: 'Initialize git.',
            task: () => initGit(options),
            enabled: () => options.git
        },
        {
            title: 'Install dependencies.',
            task: () => projectInstall({
                cwd: `${options.targetDirectory}/${options.appname}`,
            }),
            skip: () => !options.runInstall ? 'Pass -- install to automatically install dependencies.' : undefined,
        },
    ]);

    tasks.run();

    return true;
}